package task.job.gkt.andrius.vaitkus

class Manufacturer {

  long id
  String name
  static hasMany = [products: Product]

  static mapping = {
    id generator: 'sequence', params: [sequence: "seq_${this.simpleName}_id"]
    version false
  }
  static constraints = {
    name size: 5..35, blank: false, unique: true
  }
}
