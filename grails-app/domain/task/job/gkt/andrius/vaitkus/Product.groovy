package task.job.gkt.andrius.vaitkus


class Product {

  String productNr
  String description
  int quantity
  ProductGroup productGroup
  Manufacturer manufacturer

  static mapping = {
    id name: 'productNr'
    id column: 'product_nr', type: 'string'
    id generator: 'assigned', name: 'productNr'
    version false
    description type: 'text'
    quantity defaultValue: 0
  }

  static constraints = {
    productNr size: 5..35, unique: true, blank: false
    description nullable: true, blank: true
    productGroup blank: false, nullable: false
    manufacturer blank: false, nullable: false
  }
}
