package task.job.gkt.andrius.vaitkus

class BootStrap {

  def init = { servletContext ->
    ProductGroup pg1 = new ProductGroup(name: "product group1").save(faileOnError: true, flush: true)
    ProductGroup pg2 = new ProductGroup(name: "product group2").save(faileOnError: true, flush: true)
    Manufacturer m1 = new Manufacturer(name: "manufacturer1").save(faileOnError: true, flush: true)
    Manufacturer m2 = new Manufacturer(name: "manufacturer2").save(faileOnError: true, flush: true)
    new Product(productNr: "prod nr 1", description: "prod 1 description", quantity: 5, productGroup: pg1, manufacturer: m1).save(failOnError: true, flush: true)
    new Product(productNr: "prod nr 2", description: "prod 2 description", quantity: 1, productGroup: pg2, manufacturer: m2).save(failOnError: true, flush: true)
    new Product(productNr: "prod nr 3", description: "prod 3 description", quantity: 2, productGroup: pg2, manufacturer: m1).save(failOnError: true, flush: true)
    new Product(productNr: "prod nr 4", description: "prod 4 description", quantity: 6, productGroup: pg1, manufacturer: m2).save(failOnError: true, flush: true)
  }
  def destroy = {
  }
}
