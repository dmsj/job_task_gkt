/**
 * Created by Andrius on 11/2/2017.
 */
function displayErrors(map) {
  for (var key in map) {
    $("#error-" + key).html(map[key]).css("visibility", "visible");
  }
}