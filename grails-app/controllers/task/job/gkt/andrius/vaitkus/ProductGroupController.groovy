package task.job.gkt.andrius.vaitkus

class ProductGroupController {

  def productGroupService

  def create() {
  }

  def save() {
    def serverResponse = productGroupService.save(params)
    flash.message = serverResponse.message
    render(view: 'create', model: [errors: serverResponse.errors])
  }
}
