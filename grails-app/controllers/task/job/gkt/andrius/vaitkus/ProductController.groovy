package task.job.gkt.andrius.vaitkus

import commands.ProductBuyCommand
import model.ServerResponse
import org.springframework.http.HttpStatus

class ProductController {

  def productService

  static allowedMethods = [
    index      : 'GET',
    create     : 'GET',
    exportExcel: 'GET',
    show       : 'GET',
    buy        : 'POST',
    save       : 'POST'
  ]

  def buy(ProductBuyCommand product) {

    if (product == null) {
      render status: HttpStatus.NOT_FOUND
      return
    }

    def serverResponse = productService.buy(product)

    if (serverResponse.errors) {

      render(view: 'show', model: [errors: serverResponse.errors, product: serverResponse.object])
      return
    }
    flash.message = serverResponse.message
    redirect action: "index"
  }

  def show() {
    Product p = Product.findByProductNr(params.productNr)
    if (!p) {
      return new ServerResponse(message: "PRODUCT NOT FOUND")
    }
    [product: p]
  }

  def index(Integer max) {
    def groups = params.list('groups')
    def manufacturers = params.list('manufacturers')
    if (groups)
      groups = groups.collect { it as long }

    if (manufacturers)
      manufacturers = manufacturers.collect { it as long }
    //if max records per pages (max) value is not set then set default value to 5
    int m = Math.min(max ?: 5, 20)
    params.max = m
    int offset = params.offset == null ? 0 : params.offset as int
    def productList = productService.getFilteredList(groups, manufacturers)
    int maxIndex = (offset + m) > productList.size() ? productList.size() : (offset + m)
    def subList = productList.subList(offset, maxIndex)
    [products: subList, productCount: productList.size(), manufacturers: Manufacturer.list(), productGroups: ProductGroup.list(), filterByGroupList: groups, filterByManufacturerList: manufacturers]
  }

  def exportExcel() {
    productService.exportExcel(response, params, grailsApplication)
  }

  def save() {
    def serverResponse = productService.save(params)
    flash.message = serverResponse.message
    render(view: 'create', model: [errors: serverResponse.errors, manufacturers: Manufacturer.list(), productGroups: ProductGroup.list(), product: serverResponse.object])
  }

  def create() {
    [manufacturers: Manufacturer.list(), productGroups: ProductGroup.list()]
  }
}
