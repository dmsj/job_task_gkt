package task.job.gkt.andrius.vaitkus

class ManufacturerController {

  def manufacturerService

  def create() {
  }

  def save() {
    def serverResponse = manufacturerService.save(params)
    flash.message = serverResponse.message
    render(view: 'create', model: [errors: serverResponse.errors])
  }
}
