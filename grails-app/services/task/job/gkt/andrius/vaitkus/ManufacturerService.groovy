package task.job.gkt.andrius.vaitkus

import grails.gorm.transactions.Transactional
import model.ServerResponse

@Transactional
class ManufacturerService {

  /**
   * Validate and save Manufacturer object
   * @param params
   * @return ServerResponse object
   */
  def save(def params) {
    Manufacturer m = new Manufacturer(params)
    if (!m) {
      return new ServerResponse(message: "INVALID DATA")
    }
    if (!m.validate()) {
      return new ServerResponse(message: "VALIDATION ERROR", errors: m.errors)
    }
    if (!m.save(failOnError: true, flush: true)) {
      return new ServerResponse(message: "FAILED")
    }
    return new ServerResponse(message: "SAVED")
  }
}
