package task.job.gkt.andrius.vaitkus

import grails.gorm.transactions.Transactional
import model.ServerResponse

@Transactional
class ProductGroupService {

  /**
   * Validate and save ProductGroup object
   * @param params
   * @return ServerResponse object
   */
  def save(def params) {
    ProductGroup pg = new ProductGroup(params)

    if (!pg) {
      return new ServerResponse(message: "INVALID DATA")
    }
    if (!pg.validate()) {
      return new ServerResponse(message: "VALIDATION ERROR", errors: pg.errors)
    }
    if (!pg.save(failOnError: true, flush: true)) {
      return new ServerResponse(message: "FAILED")
    }
    return new ServerResponse(message: "SAVED")
  }
}
