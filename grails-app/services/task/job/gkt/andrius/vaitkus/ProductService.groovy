package task.job.gkt.andrius.vaitkus

import commands.ProductBuyCommand
import grails.gorm.transactions.Transactional
import model.ServerResponse

import java.text.SimpleDateFormat

@Transactional
class ProductService {

  def exportService
  def random = new Random()

  /**
   * Filter product list by manufacturers and product groups
   * @param groupsFilterList
   * @param manufacturersFilterList
   * @return new filtered list
   */
  def getFilteredList(def groupsFilterList, def manufacturersFilterList) {
    def criteria = Product.createCriteria()
    def result = criteria.list {
      if (groupsFilterList) {
        'in' "productGroup.id", groupsFilterList
      }
      if (manufacturersFilterList) {
        'in' "manufacturer.id", manufacturersFilterList
      }
    }

    result
  }
/**
 * Validate and executes buy command
 * @param ProductBuyCommand
 * @return ServerResponse
 */
  def buy(ProductBuyCommand command) {

    if (!command.validate()) {
      return new ServerResponse(message: "VALIDATION ERROR", errors: command.errors, object: Product.findByProductNr(command.productNr))
    }

    Product p = Product.findByProductNr(command.productNr)
    if (!p)
      return new ServerResponse(message: "PRODUCT DOESN'T EXISTS", object: p)

    p.quantity -= command.quantity

    if (!p.save(failOnError: true, flush: true)) {
      return new ServerResponse(message: "FAILED", object: p)
    }
    new ServerResponse(message: String.format("Transaction completed on %s, purchased %d product(s) by %s", command.transactionDate, command.quantity, command.buyerName == null ? "unknown" : command.buyerName), object: p)
  }

  /**
   * Exports product to excel
   * @param response
   * @param params
   * @param grailsApplication
   */
  def exportExcel(def response, def params, def grailsApplication) {
    params.exportFormat = "excel"
    params.extension = "xls"

    if (params.exportFormat && params.exportFormat != "html") {

      def products = Product.list(params)

      def results = []
      products.each { p ->
        def map = [:]
        map.put("productNr", p.productNr)
        map.put("description", p.description)
        map.put("productGroup", p.productGroup?.name)
        map.put("manufacturer", p.manufacturer?.name)
        map.put("quantity", p.quantity)

        results.add(map)
      }

      response.contentType = grailsApplication.config.grails.mime.types[params.exportFormat]
      response.setHeader("Content-disposition", "attachment; filename=products-" + new SimpleDateFormat("ddMMyyy").format(new Date()) + ".xls")

      List fields = [
        "productNr",
        "description",
        "productGroup",
        "manufacturer",
        "quantity"
      ]

      Map labels = ["productNr": "Product Nr.", "description": "Description", "productGroup": "Product Group", "manufacturer": "Manufacturer", "quantity": "Quantity"]

      Map formatters = [:]
      Map parameters = [:]

      exportService.export(params.exportFormat, response.outputStream, results, fields, labels, formatters, parameters)
    }
  }

  /**
   * Validate and save product object
   * @param params
   * @return ServerResponse object
   */
  def save(def params) {
    Product p = new Product(params)
    if (!p) {
      return new ServerResponse(message: "INVALID DATA")
    }
    //random quantity of products
    p.quantity = random.nextInt(5) + 1
    if (!p.validate()) {
      return new ServerResponse(message: "VALIDATION ERROR", errors: p?.errors, object: p)
    }
    if (Product.findByProductNr(p.productNr))
      return new ServerResponse(message: "PRODUCT NR ALREADY EXISTS", object: p)

    Manufacturer m = Manufacturer.get(params.manufacturer.id as long)
    if (!m)
      return new ServerResponse(message: "INVALID MANUFACTURER", object: p)

    p.manufacturer = m
    m.addToProducts(p)

    ProductGroup pg = ProductGroup.get(params?.productGroup?.id as long)
    if (!pg)
      return new ServerResponse(message: "INVALID PRODUCTION GROUP", object: p)

    p.productGroup = pg
    pg.addToProducts(p)
    if (!p.save(failOnError: true, flush: true, validate: true)) {
      return new ServerResponse(message: "FAILED", object: p)
    }
    return new ServerResponse(message: "SAVED", object: p)
  }

}
