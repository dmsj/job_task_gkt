<%--
  Created by IntelliJ IDEA.
  User: Andrius
  Date: 11/1/2017
  Time: 8:14 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
  <title></title>
</head>

<body>
<g:if test="${flash.message}">
  <div class="message" role="status">${flash.message}</div>
</g:if>
<form action="/manufacturer/save" method="POST">
  <fieldset class="form">
    <div class='fieldcontain required'>
      <label for='name'>Name:<span class='required-indicator'>*</span></label>
      <input type="text" name="name" id="name" required=""/>

      <p class="error-details"><g:renderErrors bean="${errors}" field="name"/></p>
    </div>

    <div class="buttons">
      <input type="submit" name="save" value="Save" id="save"/>
    </div>
  </fieldset>
</form>
</body>
</html>