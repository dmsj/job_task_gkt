<%--
  Created by IntelliJ IDEA.
  User: Andrius
  Date: 11/7/2017
  Time: 2:54 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
  <meta name="layout" content="main"/>
</head>


<body>
<div class="h2">Product info</div>

<p><i style="font-weight: bold;">Nr.:</i></i> ${product.productNr}</p>

<p><i style="font-weight: bold;">Description:</i> ${product.description}</p>

<p><i style="font-weight: bold;">Manufacturer:</i> ${product.manufacturer?.name}</p>

<p><i style="font-weight: bold;">Group:</i> ${product.productGroup?.name}</p>

<p><i style="font-weight: bold;">Quantity:</i> ${product.quantity}</p>

<form action="/product/buy" method="POST">
  <input type="hidden" name="productNr" value="${product.productNr}"/>
  <fieldset class="form">
    <div class='fieldcontain required'>
      <label for='buyerName'>Buyer Name:</label>
      <input type="text" name="buyerName" id="buyerName"/>
    </div>

    <div class='fieldcontain required'>
      <label for='transactionDate'>Transaction Date:<span class='required-indicator'>*</span></label>
      <input type="date" name="transactionDate" value="" required="" id="transactionDate"/>

      <p class="error-details"><g:renderErrors bean="${errors}" field="transactionDate"/></p>
    </div>

    <div class='fieldcontain required'>
      <label for='quantity'>Quantity<span class='required-indicator'>*</span></label>
      <input type="number" min="1" name="quantity" value="" id="quantity" required=""/>

      <p class="error-details"><g:renderErrors bean="${errors}" field="quantity"/></p>
    </div>
    <div class="buttons">
      <input type="submit" name="buy" value="Buy" id="buy"/>
    </div>
  </fieldset>

</form>

</body>
</html>