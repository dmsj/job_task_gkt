<%--
  Created by IntelliJ IDEA.
  User: Andrius
  Date: 11/1/2017
  Time: 12:01 PM
--%>

<html>
<head>
  <title></title>
  <meta name="layout" content="main"/>
</head>

<body>
<g:if test="${flash.message}">
  <div class="message" role="status">${flash.message}</div>
</g:if>
<form action="/product/save" method="POST">
  <fieldset class="form">
    <div class='fieldcontain required'>
      <label for='productNr'>Product Nr.:<span class='required-indicator'>*</span></label>
      <input type="text" name="productNr" value="${product?.productNr}" id="productNr" required=""/>

      <p class="error-details"><g:renderErrors bean="${errors}" field="productNr"/></p>
    </div>

    <div class='fieldcontain required'>
      <label for='description'>Description:</label>
      <input type="text" name="description" value="${product?.description}" id="description"/>

      <p class="error-details"><g:renderErrors bean="${errors}" field="description"/></p>
    </div>

    <div class='fieldcontain required'>
      <label for='productGroup'>Product group<span class='required-indicator'>*</span></label>
      <g:select id="productGroup" name="productGroup.id" from="${productGroups}" optionValue="name" optionKey="id"
                value="${product?.productGroup?.id}" required="" noSelection="${['null': '<--Select-->']}"/>
      <p class="error-details"><g:renderErrors bean="${errors}" field="productGroup"/></p>
    </div>

    <div class='fieldcontain required'>
      <label for='manufacturer'>Manufacturer<span class='required-indicator'>*</span></label>
      <g:select id="manufacturer" name="manufacturer.id" from="${manufacturers}" optionValue="name" optionKey="id"
                value="${product?.manufacturer?.id}" required="" noSelection="${['null': '<--Select-->']}"/>
      <p class="error-details"><g:renderErrors bean="${errors}" field="manufacturer"/></p>
    </div>
    <div class="buttons">
      <input type="submit" name="save" value="Save" id="save"/>
    </div>
  </fieldset>

</form>

</body>

</html>