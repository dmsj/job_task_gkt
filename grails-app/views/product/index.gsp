<%--
  Created by IntelliJ IDEA.
  User: Andrius
  Date: 11/3/2017
  Time: 12:01 PM
--%>

<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

%{--</form>--}%
<form action="/product/index" method="GET">
  <fieldset class="form">
    <table style="width: 400px;">
      <tr>
        <td>
          <label>Product groups</label>
        </td>
        <td>
          <label>Manufacturers</label>
        </td>
      </tr>
      <tr>
        <td>
          <g:each in="${productGroups}" var="group">
            <p>${group.name}</p><g:checkBox name="groups" value="${group.id}"
                                            checked="${group.id in filterByGroupList}"/>
          </g:each>
        </td>
        <td>
          <g:each in="${manufacturers}" var="manufacturer">
            <p>${manufacturer.name}</p><g:checkBox name="manufacturers" value="${manufacturer.id}"
                                                   checked="${filterByManufacturerList?.contains(manufacturer.id)}"/>
          </g:each>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="filter" value="Filter" id="filter"/></td>
      </tr>
    </table>

  </fieldset>

</form>
<a href="#list-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div id="list-employee" class="content scaffold-list" role="main">
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table>
    <tr>
      <td>Product nr</td>
      <td>Description</td>
      <td>Group</td>
      <td>Manufacturer</td>
      <td>Action</td>
    </tr>

    <g:each in="${products}" var='p'>
      <tr>
        <td>${p.productNr}</td>
        <td>${p.description}</td>
        <td>${p.productGroup?.name}</td>
        <td>${p.manufacturer?.name}</td>
        <td><g:link action="show" params="[productNr: p.productNr]">Buy</g:link></td>
      </tr>
    </g:each>
  </table>


  <div class="pagination">
    <g:paginate total="${productCount ?: 0}"
                params="${[groups: filterByGroupList, manufacturers: filterByManufacturerList]}"/>
  </div>
  <br>
  <br>

  <div class="nav" role="navigation">
    <ul>
      <li><g:link class="create" action="exportExcel">Export to Excel</g:link></li>
    </ul>
  </div>
</div>
</body>
</html>