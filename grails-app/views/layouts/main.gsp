<!doctype html>
<html lang="en" class="no-js">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <title>
    <g:layoutTitle default="Grails"/>
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <asset:stylesheet src="application.css"/>

  <g:layoutHead/>
</head>

<body>

<div class="navbar navbar-default navbar-static-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/#">
        <i class="fa grails-icon">
          <asset:image src="grails-cupsonly-logo-white.svg"/>
        </i> Grails
      </a>
    </div>

    <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">
      <ul class="nav navbar-nav navbar-right">
        <g:pageProperty name="page.nav"/>
      </ul>
    </div>
  </div>
</div>

<div class="nav" role="navigation">
  <ul>
    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    <li><g:link class="create" controller="product" action="create">Add product</g:link></li>
    <li><g:link class="create" controller="productGroup" action="create">Add product group</g:link></li>
    <li><g:link class="create" controller="manufacturer" action="create">Add manufacturer</g:link></li>
    <li><g:link class="list" controller="product" action="index">All products</g:link></li>
  </ul>
</div>
<g:layoutBody/>

<div class="footer" role="contentinfo"></div>

<div id="spinner" class="spinner" style="display:none;">
  <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
