package model

/**
 * Created by Andrius on 11/2/2017.
 */
class ServerResponse {
  String message
  def errors
  def object
}
