package commands

import grails.databinding.BindingFormat
import grails.validation.Validateable
import task.job.gkt.andrius.vaitkus.Product

/**
 * Created by Andrius on 11/7/2017.
 */
class ProductBuyCommand implements Validateable {
  String productNr
  int quantity
  @BindingFormat('yyyy-MM-dd')
  Date transactionDate
  String buyerName

  static constraints = {
    quantity min: 1, validator: { value, object ->
      Product p = Product.findByProductNr(object.productNr)
      if (p == null || p.quantity < value) {
        return ['tooLow']
      }
      return true
    }
    buyerName nullable: true
    transactionDate blank: false, validator: { value, object ->
      if (value.getTime() > System.currentTimeMillis()) {
        return ['futureDate']
      }
      return true
    }

  }
}