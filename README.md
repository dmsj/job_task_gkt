### What is this repository for? ###

Simple grails CRUD operations.

### Required tools ###

PostgreSQL 10.1
Grails 3.3.1
JDK 1.8

### Contribution guidelines ###

Change DB login and password in application.yml config file

